Shrike
======

Shrike is a game/toy/tool in the planning stages, aiming to be a simple programmable robot simulation, where users should can write code to control a robot to solve puzzles or complete tasks.

Inspiration is taken from [rubywarrior](https://www.bloc.io/ruby-warrior#/) (a game where you write ruby to complete small AI tasks), [Stormrunner](https://github.com/korikisulda/stormrunner) (a LEGO Mindstorms game about exploring another planet by building and controlling robots after crash landing, no longer available), and the [Roamer robot](http://www.valiant-technology.com/uk/pages/roamer_home.php).

More information on the design and planned features can be found on [the wiki](https://github.com/borntyping/shrike/wiki).

Usage
-----

Shrike uses [LÖVE](http://love2d.org/). This can be installed (on Ubuntu based systems) with these commands:

    sudo add-apt-repository ppa:bartbes/love-stable
    sudo aptitude update
    sudo aptitude install love

Shrike can then be run with:

    love ./src

Author
------

- Sam Clements <sam@borntyping.co.uk>
