game = {}

function game.load_tileset(path, width, height)
  local tileset = love.graphics.newImage(path)
  local quads = {}

  for x=0,(tileset:getWidth() / width) do
    quads[x] = {}
    for y=0,(tileset:getHeight() / height) do
      quads[x][y] = love.graphics.newQuad(
        x * width,  y * height, width, height,
        tileset:getWidth(), tileset:getHeight())
    end
  end

  return tileset, quads
end

function game.loadmap(path)
  local tiles = {
    {' ', 0, 0},
    {'#', 1, 0},
    {'x', 0, 1},
    {'^', 1, 1}
  }

  local data = love.filesystem.read(path)
  local width = #(data:match("[^\n]+"))

  local map = {}

  for i=1,width do
    map[i] = {}
  end

  row = 1
  for row_data in data:gmatch("[^\n]+") do
    assert(#row_data == width)
  end

  print(data)


  return map
end


function love.load()
  tileset, quads = game.load_tileset('data/countryside.png', 32, 32)
  map = game.loadmap('data/map.txt')

  tiles = {
    grass=quads[0][0],
    flowers=quads[0][1],
    box=quads[1][0],
    boxtop=quads[1][1],
  }

  -- map = {
  --   { 'grass', 'grass', 'grass' },
  --   { 'grass', 'flowers', 'flowers', 'flowers' },
  --   { 'grass', 'flowers', 'box',     'flowers' },
  --   { 'grass', 'flowers', 'flowers', 'flowers' },
  -- }

  map = {
    { 4,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4 },
    { 4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,1,4 },
    { 4,1,3,1,1,1,1,1,1,1,1,1,1,1,1,3,1,1,1,1,1,1,1,1,4 },
    { 4,1,1,1,1,1,1,1,2,1,1,2,1,1,1,1,1,1,1,1,1,1,1,1,4 },
    { 4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,4 },
    { 4,1,1,4,1,1,1,1,1,2,2,1,1,4,1,1,1,4,1,4,2,2,1,1,4 },
    { 4,1,1,4,1,1,1,1,4,3,3,4,1,2,1,1,1,2,1,4,1,1,1,1,4 },
    { 4,1,1,4,1,1,1,1,4,3,3,4,1,1,4,1,4,1,1,4,2,2,1,1,4 },
    { 4,1,1,4,1,1,1,1,4,3,3,4,1,1,2,1,2,1,1,4,1,1,1,1,4 },
    { 4,1,1,4,1,1,1,1,2,3,3,2,1,1,1,4,1,1,1,4,1,1,1,1,4 },
    { 4,1,1,2,2,2,2,1,1,2,2,1,1,1,1,2,1,3,1,2,2,2,1,1,4 },
    { 4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,4 },
    { 4,1,1,1,1,1,1,1,1,2,1,1,1,1,2,2,4,1,1,1,1,1,1,1,4 },
    { 4,1,1,1,1,1,1,1,4,3,4,1,1,1,1,1,2,1,1,1,1,1,1,1,4 },
    { 4,1,1,3,1,1,1,1,2,3,2,1,1,1,1,2,1,1,1,1,1,1,1,1,4 },
    { 4,1,1,1,1,1,1,1,1,2,1,1,2,1,2,1,1,1,1,1,1,1,3,1,4 },
    { 4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,4 },
    { 2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2 }
  }

  -- 18 x 25\

  maptiles = {
    tiles['grass'],
    tiles['box'],
    tiles['flowers'],
    tiles['boxtop']
  }
end

function love.keypressed(key)
  if key == 'escape' then
    love.event.push('quit')
  end
end


function love.draw()
  local width, height = love.window.getDimensions()
  for x=0,(width/32)+1 do
    for y=0,(height/32)+1 do
      if map[y] and map[y][x] then
        love.graphics.draw(tileset, maptiles[map[y][x]], (x - 1) * 32, (y - 1) * 32)
      else
        love.graphics.draw(tileset, maptiles[1], (x - 1) * 32, (y - 1) * 32)
      end
    end
  end

  love.graphics.printf("FPS: " .. love.timer.getFPS(), love.graphics.getWidth() - 120, 20, 100, 'right')
end
