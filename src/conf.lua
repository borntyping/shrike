require('vendor/cupid')

function love.conf(t)
    t.version = "0.9.1"

    t.window.title = "Shrike"
    t.window.resizable = true
    t.window.minwidth = 100
    t.window.minheight = 100
end
